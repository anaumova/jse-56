package ru.tsc.anaumova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.anaumova.tm.component.Bootstrap;
import ru.tsc.anaumova.tm.configuration.ContextConfiguration;

public final class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ContextConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}