package ru.tsc.anaumova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.dto.request.UserChangePasswordRequest;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), password);
        getUserEndpoint().changePassword(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}