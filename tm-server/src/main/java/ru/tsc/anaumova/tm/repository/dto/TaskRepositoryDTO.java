package ru.tsc.anaumova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;
import ru.tsc.anaumova.tm.enumerated.Sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public class TaskRepositoryDTO extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull String userId, @NotNull Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull String userId, @NotNull Comparator<TaskDTO> comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        Optional<TaskDTO> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        Optional<TaskDTO> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM TaskDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(e) FROM TaskDTO e";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(e) FROM TaskDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(String projectId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        entityManager.createQuery(jpql)
                .setParameter("userId", TaskDTO.class)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}